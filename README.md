
## The Goal ##

Given a triangular mesh and a point in space, find the closest distance of the query point to the mesh.

Assumption: The closest point on the mesh is not just the vertices of the mesh, but the entire mesh surface, including edges, vertices, and triangular faces.

## Method ##

Since the problem is highly parallelizable, one approach to this problem is to run a brute-force algorithm, which is looping through all primitives(vertex, edge, and face) and finding a point on the mesh which is closest to the query point, but run it parallel. Since most of the computations for each primitive can be done independently from the rest of the primitives, parallel computing can find the solution fast.

Finding distances are straightforward. The distance between vertex to point is just the Euclidean distance between them. A closest distance of a point to an edge is the projection of that point on the line of the edge. Then checking if the point lies on the the edge segment of that line. And the closest distance of a point to the triangular face is the projection of the point to the plane of the triangle and check if the point is located inside the triangle segment using the barycentric weights. 

After finding the distances we can get the minimum of all distances calculated. 


## Code details ##

#### Requirements: ####
[Ply IO](https://people.sc.fsu.edu/~jburkardt/data/ply/ply.html)The two files ply.h and ply.c that is required to run the code is included in the /src/ folder.

Eigen C++ library for 3D linear algebra calculations

Boost C++ library for testing

OpenMP C++ library for parallization 

Inputs: Mesh file(.ply) and a file containing the position of query point with the search radius(.txt) 




#### Structures: ####

There are three types of objects: 

* Mesh: This contains information about the mesh. Given a .ply mesh file, it reads the mesh file and returns the list of vertices, edges, and faces with their sizes.

* Face: This contains information about triangular faces such as the three vertices and the ids with the id that identifies the face.

* ClosestPointQuery: This contains all the functionalities needed for finding the closest point on the mesh. 

There are distance functions that find the distances of the point to the primitives and the results are saved as a pair in which the first element is the distance and the second element is the point that is located in the primitives. All pairs are saved in a set type of data which makes the computation faster.

The main function that finds the closest point on the mesh given a point and maximum radius of search is an overloaded operator () in ClosestPointQuery class ( ClosestPointQuery::operator ()(const Vector3f &point, float maxRadius)) . It calls 3 functions that each returns the closest vertex, edge, and face to the point and return the closest among these three points, which is the final result.

## Testing ##

The main.cpp file contains test cases for helper functions that find distances to the vertex, edge, and face. It also tests the whole algorithm. Given a mesh and maximum search radius it test for two properties: 1. If the result is in the search radius/sphere.  2. If the resulting point is located on the mesh.

I used the Chrono framework in C++ to time the functions and compute the total runtime. The total runtime for the bunny.ply with vertex 35947 vertices, 208353 edges and 69451 faces is 1.518 seconds on 2.9 GHz Dual-Core Intel Core i7 personal mac computer. 



## How to use the code ##

You can clone the source code:

> git clone https://zahrafn@bitbucket.org/zahrafn/closestpointquery.git

And inside the folder:

> mkdir build

> cmake ..

> make -j

> ./main 

--This runs with the default mesh (/data/bunny.ply) and query point and a search radius(/data/queryPointsBunny.txt) )

One can also pass a mesh file and a text file containing the query point and search radius such as:

> ./main ../data/simpleCube.ply ../data/queryPointsCube.txt

NOTE: Make sure to adjust the CMakeFile with your own references


I have used /data/simpleCube.ply with /data/queryPointsCube.txt for testing in more detail. I used a triangular mesh of a 2 by 2 cube located at the center of xyz frame and set a point in different positions to check the details of distances and location of projected points with what I expected.   
  
