#include "Mesh.h"
#include <iostream>
#include <exception>
#include <map>
#include <numeric>
using namespace Eigen;

Mesh::Mesh(char* filename){
	
	if (!readPly(filename)){
		cout <<"Failed to read ply file "<< filename<<endl;
		char* defultPly =  "../data/cubeTriangle.ply";
		readPly(defultPly);
		cout <<"Reading the defult Ply file: "<< defultPly <<endl;
	}else{
		make_edgeList();
		cout << "Reading Ply file: "<<filename<<endl;
	}
        //! Make edge list from vertex and face list given by Ply reader,               
}

Mesh::Mesh(const Mesh &obj){
	vertex_list = obj.vertex_list;
	face_list = obj.face_list;
	edge_list = obj.edge_list;
	nverts = obj.nverts;
	nfaces = obj.nfaces;
	nedges = obj.nedges;
}


//! Using Ply file lib to populate vertex and face lists
bool Mesh::readPly(char *filename){

	typedef struct face_struct {

		unsigned char nverts;
  		int *verts;
  		float nx,ny,nz;
		} face_struct;

	typedef struct Vertex {
  		float x,y,z;             /* the usual 3-space position of a vertex */
		} Vertex;
	
	int vertices;
	PlyFile *plyFile;
	PlyElement **elems;
	PlyProperty *property,**ele_prop;
	FILE *fp;

	PlyProperty vert_props[] = {
		{"x", Float32, Float32, offsetof(Vertex,x), 0, 0, 0, 0},
  		{"y", Float32, Float32, offsetof(Vertex,y), 0, 0, 0, 0},
  		{"z", Float32, Float32, offsetof(Vertex,z), 0, 0, 0, 0},
	};

	PlyProperty face_props[] = {
  		{"vertex_indices",Uint8 , Int32, offsetof(face_struct,verts), 1, Uint8, Uint8,offsetof(face_struct,nverts)}
	};

	float arr;
	int numOfElements,nprops,i,k,j;
	char *ele_name;
	char **elements;

  	fp = fopen(filename,"r");
  	plyFile = read_ply(fp);
  	if (fp == NULL){
  		cout<< "Error reading ply file "<< filename<<endl;
  		return false;
  	}

	try{
  		for(i=0; i< plyFile->num_elem_types; i++){
			elems = plyFile->elems;
	 		ele_name = elems[i]->name;
	 		numOfElements=elems[i]->num;

		 	cout<< "Number of "<< ele_name<< ": "<<elems[i]->num<<endl;
	 		nprops = elems[i]->nprops;
	 		ele_prop = elems[i]->props;
	 		//cout<<ele_prop[0]->name;

	  		if(equal_strings ("vertex", ele_name)){
				vertices=elems[i]->num;        
				//vlist = (Vertex **) malloc (sizeof (Vertex *) * elems[i]->num);

				ply_get_property (plyFile, ele_name, &vert_props[0]);
				ply_get_property (plyFile, ele_name, &vert_props[1]);
				ply_get_property (plyFile, ele_name, &vert_props[2]);               

				Vertex* vlist = (Vertex*) malloc (sizeof (Vertex));
				Vector3f temp;
				for (j = 0; j < numOfElements; j++) {
	  
		 			 ply_get_element (plyFile, (void *) vlist);
		
					temp[0] = vlist->x;
					temp[1] = vlist->y;
					temp[2] = vlist->z;

	  				vertex_list.push_back(temp);
	  				//cout << vertex_list[j].x << vertex_list[j].y << vertex_list[j].z<<endl;
	   			}
	 		}

  			if (equal_strings ("face", ele_name)) {	  
				ply_get_property (plyFile, ele_name, &face_props[0]);
				//cout << "face_props" << face_props[0];
				face_struct *flist = (face_struct*) malloc (sizeof (face_struct));
				Face face_obj;
				int faces = elems[i]->num;
				for (j = 0; j < elems[i]->num; j++) {
	  				ply_get_element (plyFile,(void*)flist);

	  				//cout << "face" << (float)flist->verts[0]<<endl;
		 			face_obj.nverts = (float)flist->nverts;
		 			face_obj.vid = flist->verts;
		 			face_obj.v1 = vertex_list[(int)flist->verts[0]];
		 			face_obj.v2 = vertex_list[(int)flist->verts[1]];
		 			face_obj.v3 = vertex_list[(int)flist->verts[2]];
		 			face_obj.id = j;
		 			face_list.push_back(face_obj);
		 			//cout << "id" <<face_obj.vid[0]<<face_obj.vid[2]<<endl;
	 			}
  			}
  		}
  		nverts = vertex_list.size();
  		nfaces = face_list.size();
 	}
 	catch(exception& e){
 		cout << "Hit exception during ply parse, only ASCII formats supported "<<e.what();
 	}
  	return true;
}

void Mesh::make_edgeList(){

        int size = nverts;
        set <int> temp;
        std::vector<set <int> > edge_list;
	std::vector<int> num_edges(size);
	num_edges.reserve(size);
        vector<Face> face_list = get_faceList();

        for(int i = 0; i< size; i++)
                edge_list.push_back(temp);

        //! Make adjacency list by looping through all faces
        for (int i =0 ; i< face_list.size(); i++){
                int n = face_list[i].nverts;
                //std::vector<set <int> > :: iterator it;
                //! Each face stores vertex ID list, convert it to 3 edges              
		for(int j = 0; j< n;  j++){
                        int idx1 = face_list[i].vid[j];
                        int idx2 = face_list[i].vid[(j+1) % n];
                        edge_list[idx1].insert(idx2);
			num_edges[idx1] += 1;
		}
        }
	nedges = accumulate(num_edges.begin(), num_edges.end(), 0);
	cout<< "Number of nedges: "<< nedges <<endl;
        set_edgeList(edge_list);
};

