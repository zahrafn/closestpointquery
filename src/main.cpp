#include "ClosestPointQuery.h"
#include <fstream>
#include <cmath>
#include <boost/test/minimal.hpp>

//! Test cases for testing the ditance helper functions: dist_to_vertex, dist_to_edge, test_to_face
int test_distance_functions(){

	Vector3f v0(0,1,0), p1(0,4,0);
	float d = dist_to_vertex(v0,p1 );
	cout << "T1. dist to vertex: "<< d  << endl;
	BOOST_CHECK(dist_to_vertex(v0,p1 ) == 3);

	Vector3f v1(0,1,0), v2(0,-1,0);
	Vector3f p2(2,0,0);
	pair<float,Vector3f> h1;
	dist_to_edge(v1,v2,p2,h1);
        cout << "T2. dist to edge: "<< h1.first << " at point: " << h1.second[0]<<","<<h1.second[1]<<","<<h1.second[2]<< endl;
	BOOST_CHECK( h1.first == 2);

	Vector3f v11(1,0,0), v22(0,1,0);
	Vector3f p22(1,1,0);
	float expected = sqrt(2)/2;
	pair<float,Vector3f> h2;
	dist_to_edge(v11,v22,p22,h2);
        cout << "T3. dist to edge : "<< h2.first <<" at point: " << h2.second[0]<<","<<h2.second[1]<<","<<h2.second[2]  << endl;
	BOOST_CHECK( h2.first == expected);



        Vector3f a(0,1,0), b(-2,-1,0), c(2,-1,0);
	Vector3f p3(0,0,2);
	pair<float,Vector3f> h3;
        dist_to_face(a,b,c,p3,h3);
        cout << "T4. dist to face: "<< h3.first <<" at point: " << h3.second[0]<<","<<h3.second[1]<<","<<h3.second[2]<< endl;
	BOOST_CHECK( h3.first == 2);
	
        return 1;

}

//! Test case to validate if the point is in bounds
bool pointInSphere(Vector3f result, Vector3f queryPoint, float radius){
 	return (pow(result[0]-queryPoint[0],2) + 
 			pow(result[1]-queryPoint[1],2) + 
 			pow(result[2]-queryPoint[2],2) <= pow(radius,2));
}

//! Test case to check if the point actually exists on the mesh
bool pointOnMesh(Vector3f output,Mesh m){
	vector<Face> face = m.get_faceList();
	for (int i=0; i<face.size() ; i++){
		Face temp = face[i];
		if(insideTriangle(temp.v1,temp.v2,temp.v3,output))
			return true;
	}
 	return false;
}

//! Testing the algorithm on a given meth
int test_main(int argc, char** argv){
	
	cout << "---------------- Testing helper functions :";
	cout <<" (checking distance of a point to a Vertex, Edge and face)"<<endl;
	test_distance_functions();	
	
	cout << "---------------- Testing algorithm a given mesh "<< endl;
	char* plyfile;
	char* pointfile;
	if(argc < 2){
		cout << "Not enough inputs are given ";
		cout << "Example: > ./main <MESH_FILE.ply> <POINT_FILE.txt>" << endl;;
		plyfile = "../data/bunny.ply";
		pointfile = "../data/queryPointsBunny.txt";
		cout << "Reading the Defult mesh and point files: "<< plyfile <<" and  " << pointfile << endl;
	}
	else{
		plyfile = argv[1];
		pointfile = argv[2];
	}	

	Mesh mesh(plyfile);
	ClosestPointQuery cpq = ClosestPointQuery(mesh);  
		
	//! Read query points from a file
	std::ifstream infile(pointfile);
	float x,y,z,searchRadius;
	
	while(infile >>x >> y >> z >> searchRadius){		
		
		Vector3f queryPoint = Vector3f(x,y,z);
		Vector3f output = cpq(queryPoint,searchRadius);	

		cout << "1-Check if the point is inside the limit: " << pointInSphere(output,queryPoint,searchRadius) << endl;			
		BOOST_CHECK(pointInSphere(output,queryPoint,searchRadius) == true);

		cout << "2-Chek if the point is on the mesh " << pointOnMesh(output,cpq.getMesh()) << endl;
		BOOST_CHECK(pointOnMesh(output,cpq.getMesh()) == true);
	}	

	return 0;
};

