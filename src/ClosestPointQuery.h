#include "Mesh.h"
#include <math.h>
#include <limits>

#define F_INFINITY std::numeric_limits<float>::infinity()

using namespace std;


class ClosestPointQuery{
private:
	Mesh mesh;

public:
	
	ClosestPointQuery(const Mesh &m){
		mesh = Mesh(m);
		std::vector<set <int> > edge_list = mesh.get_edgeList();		
	};

	~ClosestPointQuery(){};

	//! Overload the call () operator
	Vector3f operator ()(const Vector3f &point, float maxRadius);
	
	//! Closest vertex to the query Point
	pair<float,Vector3f> closestVertex(const Vector3f &point, float maxRadius);

	//! Closest point on an edge, from all edges in  the mesh
	pair<float,Vector3f> closestEdge(const Vector3f &point, float maxRadius);

	//! Closest point on a face, from all faces in  the mesh
	pair<float,Vector3f> closestFace(const Vector3f &point, float maxRadius);
	
	inline Mesh getMesh(){
		return mesh;
	}

};

bool insideTriangle( const Vector3f &v1, const Vector3f &v2, const Vector3f &v3,const Vector3f & p);
//! Helper functions to get the hit point and the distances to the vertex, edge and face
float dist_to_vertex(const Vector3f &v, const Vector3f &p);
void dist_to_edge(const Vector3f &v1, const Vector3f &v2, const Vector3f &p, pair<float,Vector3f> &hit);	
void dist_to_face(const Vector3f &v1, const Vector3f &v2, const Vector3f &v3, const Vector3f &p, pair<float,Vector3f> &hit);	

