#pragma once

#include <iostream>
#include <stdlib.h>
#include <vector>
#include <set>
#include "Face.h"
#include "ply.h"

using namespace std;
using std::vector;

class Mesh{

private:
	vector<Vector3f> vertex_list;
	vector<Face> face_list;
	std::vector<set <int> > edge_list;

public:
	int nverts,nfaces, nedges;
	Mesh(){};
	Mesh(char* filename);
	Mesh(const Mesh &obj);	
	~Mesh(){};
	
	inline vector<Vector3f> get_vertexList(){
		return vertex_list;
	}

	inline vector<Face> get_faceList(){
		return face_list;
	}

	inline std::vector<set <int> > get_edgeList(){
		return edge_list;
	}
	inline void set_edgeList(std::vector<set <int> > e){
		edge_list = e;
	}
	
	void make_edgeList();	        //! Adjacency List for edges from Vertex and Face list

	inline int get_numEdges(){
		return nedges;
	}
	
	bool readPly(char* plyfile);
};
