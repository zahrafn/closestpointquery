#include "ClosestPointQuery.h"
#include <utility>
#include <set>
#include <algorithm>
#include <time.h>
#include <math.h> 
#include <typeinfo>
#include <chrono>

#define INFINITY_FLOAT std::numeric_limits<float>::infinity();
#define TIMEIT true //! set to false for no timing

//! -------------------------------------- Compare function for sorting  
bool comp(const std::pair<float,Vector3f>& firstElem, const std::pair<float,Vector3f>& secondElem){	
	return firstElem.first < secondElem.first;
}
//! --------------------------------------

Vector3f ClosestPointQuery::operator () (const Vector3f &queryPoint, float maxRadius){
	
	cout<< "--------------------------- Query Point: " << queryPoint[0] <<" "<< queryPoint[1] << " "<< queryPoint[2];
	cout <<" Max Radius: " << maxRadius<<endl;

	vector< pair <float,Vector3f> > closestPoints;
	
	std::chrono::time_point<std::chrono::system_clock> start_init, end_finished;
	std::chrono::time_point<std::chrono::system_clock> start, end;
	std::chrono::duration<double> elapsed_seconds;

	//! Get the closest vertex to the query point 
	if(TIMEIT){	
   		start_init = std::chrono::system_clock::now();}
		start = start_init;	
	closestPoints.push_back( closestVertex(queryPoint,maxRadius));
	if(TIMEIT){	
		end = std::chrono::system_clock::now();
		elapsed_seconds = (end - start)*1000.0;	
		cout << " Time to check all vertecies: " << elapsed_seconds.count()<<" (ms)" << endl;
		cout << endl;}

	//! Get the closest egde to the query point 
	if(TIMEIT)
		start = std::chrono::system_clock::now();
	closestPoints.push_back( closestEdge(queryPoint,maxRadius));		
	if(TIMEIT){
		end = std::chrono::system_clock::now();
		elapsed_seconds = (end - start)*1000.0;
		cout << " Time to check all edges: " << elapsed_seconds.count()<<" (ms)"<< endl;
        	cout << endl;}

	//! Get the closest face to the query point 
	if(TIMEIT)
		start = std::chrono::system_clock::now();
	closestPoints.push_back( closestFace(queryPoint,maxRadius));
	if(TIMEIT){
		end = std::chrono::system_clock::now();
		elapsed_seconds = (end - start)*1000.0;
		cout << " Time to check all faces: " << elapsed_seconds.count()<<" (ms)"<< endl;
        	cout << endl;}

	std::sort(closestPoints.begin(), closestPoints.end(),comp);
	float minDist = closestPoints[0].first;
	Vector3f result = closestPoints[0].second;

	

	if (isnan(result[0]) || isnan(result[1]) || isnan(result[2]))
		cout<< " *** No closest point on mesh found within given maxRadius"<<endl;
	else{
		cout <<" *** CLOSEST POINT ON MESH AT: "<< result[0]<< " "<< result[1]<< " "<< result[2];
		cout << ", DISTANCE " << minDist<< endl;}
	
	if(TIMEIT){
                end_finished = std::chrono::system_clock::now();
		elapsed_seconds = (end_finished - start_init)*1000.0;
		cout << " TOTAL RUN TIME : " << elapsed_seconds.count()<<" (ms)"<< endl;}
	cout<<endl;
	return result;
};

 //! -------------------------------------- Helper functions
bool insideTriangle(const Vector3f &v1, const Vector3f &v2, const Vector3f &v3,const Vector3f & p){
 
	//! compute normal to triangle
    	Vector3f v12 = v2 - v1; 
    	Vector3f v13 = v3 - v1; 
    	Vector3f n = v12.cross(v13); 
	Vector3f c;
    	//! edge 1
    	Vector3f edge1 = v2 - v1;
    	Vector3f vp1 = p - v1;
    	c = edge1.cross(vp1);
    	if (n.dot(c) < 0)
		return false;
    	//! edge 2
    	Vector3f edge2 = v3 - v2; 
    	Vector3f vp2 = p - v2; 
    	c = edge2.cross(vp2); 
    	if (n.dot(c) < 0)
		return false; 
    	//! edge 3
    	Vector3f edge3 = v1 - v3; 
    	Vector3f vp3 = p - v3; 
    	c = edge3.cross(vp3); 
    	if (n.dot(c) < 0)
		return false; 
 
	return true;
}


//! -------------------------------------- Distance helper functions

float dist_to_vertex(const Vector3f &v, const Vector3f &p){
	return  sqrt(pow(v[0]-p[0],2) + pow(v[1]-p[1],2) + pow(v[2]-p[2],2)); 
};

void dist_to_edge(const Vector3f &v1,const Vector3f &v2, const Vector3f &p, pair <float,Vector3f> &hit){	

	Vector3f v = v2-v1;
	Vector3f w = p-v1;
	
	float c1 = w.dot(v);
	float c2 = v.dot(v);
	float t = c1/c2;

	Vector3f hpoint = v1 + t*v;
	hit.second = hpoint;
	if (c1>0 && c2>=c1){
		hit.first = dist_to_vertex(hpoint, p);
	}
         else
                hit.first = INFINITY_FLOAT;
};


void dist_to_face(const Vector3f &v1,const Vector3f &v2,const Vector3f &v3, const Vector3f &p, pair <float,Vector3f> &hit){
	
	Vector3f e0 = v2 - v1;
	Vector3f e1 = v3 - v1;
	Vector3f d = v1 - p;
		
	Vector3f n = e0.cross(e1);
	n.normalize();
	Vector3f projected_point = p + d.dot(n)*n;

	hit.second = projected_point;
	if(insideTriangle(v1,v2,v3, projected_point)){
		hit.first = dist_to_vertex( projected_point, p);
	} 
	else
		hit.first = INFINITY_FLOAT;

};

//! -------------------------------------- Closest point on each obj(vertex, Edge and Face)

pair<float,Vector3f> ClosestPointQuery::closestVertex(const Vector3f &point, float maxRadius){

	float d;
	vector<Vector3f> vertex_list = mesh.get_vertexList();
	int v_size = vertex_list.size();
	vector<pair <float,Vector3f>> allHits;
	allHits.reserve( v_size );
	
	#pragma omp parallel for
	for(int i=0; i< v_size ;i++){
		pair <float,Vector3f> hit;
		hit.first = dist_to_vertex(vertex_list[i], point);		
		//cout << "dist: " << dist_to_vertex(vertex_list[i], point) << " geting: "<< hit.first << endl;
		hit.second = vertex_list[i];
		if (hit.first >= maxRadius){
                        hit.first = INFINITY_FLOAT;
                }
		allHits[i] = hit;
		//cout << " Hit V  " << allHits[i].first  << " p:"<<allHits[i].second[0] <<" "<< allHits[i].second[1] <<" "<< allHits[i].second[2] << endl;

	}
	std::sort(allHits.begin(), allHits.end()+v_size, comp);
        Vector3f closest = allHits[0].second;
        float minDist = allHits[0].first;
	cout << " Closest Vertex to Query Point " << closest[0] <<" "<< closest[1] <<" "<< closest[2];
	cout << ", Distance " << minDist<< endl;

	return std::pair<float,Vector3f>(minDist,closest);
};

pair<float,Vector3f> ClosestPointQuery::closestEdge(const Vector3f &point,float maxRadius){

	int e_idx = 0;
	int v1,v2;
	vector<Vector3f> vertex_list = mesh.get_vertexList();
	std::vector<set <int> > edge_list = mesh.get_edgeList();
	int e_size = mesh.get_numEdges();//nedges;	
	vector<pair <float,Vector3f>> allHits;
	allHits.reserve( e_size );
	
	#pragma omp parallel for
	for (int i =0; i< edge_list.size(); i++){
		
		Vector3f v1 = vertex_list[i];
		set <int> temp = edge_list[i];		

		for (set<int>::iterator j = temp.begin(); j != temp.end(); j++) {
   			pair <float,Vector3f> hit;
			int element = *j;
			Vector3f v2 = vertex_list[element];
			dist_to_edge(v1,v2,point, hit);
			if (hit.first >= maxRadius){
                		hit.first = INFINITY_FLOAT;
			}
			allHits[e_idx] = hit;
			e_idx += 1;
		}		
	}
	std::sort(allHits.begin(), allHits.end()+e_size, comp);
	Vector3f closest = allHits[0].second;
	float minDist = allHits[0].first;
	cout << " Closest Point on Edge to Query Point " << closest[0] <<" "<< closest[1] <<" "<< closest[2];
	cout << ", Distance " <<  minDist <<endl;
	return pair<float,Vector3f> (minDist,closest);
};


pair<float,Vector3f> ClosestPointQuery::closestFace(const Vector3f &point, float maxRadius){

		
	std::vector<Face> faces_list;	
	faces_list = mesh.get_faceList();
	
	int f_size = faces_list.size();
	vector<pair <float,Vector3f>> allHits;
	allHits.reserve( f_size );
	
	#pragma omp parallel for
	for(int i = 0; i<f_size; i++){
		pair <float,Vector3f> hit;
		allHits[i] = hit;
		Vector3f v1 = faces_list[i].v1;
		Vector3f v2 = faces_list[i].v2;
		Vector3f v3 = faces_list[i].v3;
		dist_to_face(v1, v2, v3, point, hit);
		if (hit.first >= maxRadius){
			hit.first = INFINITY_FLOAT;
		}
		allHits[i] = hit;
	}	
	std::sort(allHits.begin(), allHits.end()+f_size, comp);
        Vector3f closest = allHits[0].second;
        float minDist = allHits[0].first;
	cout << " Closest Point on Face to Query Point: " << closest[0] << " "<< closest[1] << " "<< closest[2];
	cout<<", Distance " << minDist <<endl ;	
	return std::pair<float,Vector3f>(minDist,closest);
};


